class DienThoai {
  constructor(id, name, image, description, price, inventory, rating, type) {
    this.id = id;
    this.name = name;
    this.image = image;
    this.description = description;
    this.price = price;
    this.inventory = inventory;
    this.rating = rating;
    this.type = type;
  }
  render() {
    return `
      <div class="col-4">
       <div class="card p-2">
          <img style="height:280px;"
              src="${this.image}"
              class="w-100" alt="">
            <p>Tên sản phẩm : ${this.name}</p>
            <p>Mã sản phẩm : ${this.id}</p>
            <p>Số lượng : ${this.inventory}</p>
            <p>Rating : ${this.rating}</p>
            <p>Loại : ${this.type}</p>
              
          <p>
              Mô tả :${this.description}
          </p>
          <button class="btn btn-danger" onclick="deleteProduct(${this.id})">
          $${this.price} nhấn để xóa khỏi database
          </button>
          <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal2" onclick="getUpdateProduct(${this.id})">
          Chỉnh sửa sản phẩm 
          </button>
      </div>
      </div>
      `;
  }
}
