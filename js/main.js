let productList = [];
//function 1 : lay san pham tu db
const fetchProducts = () => {
  productList = [];
  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      mapData(res.data);

      //render san pham ra man hinh
      renderProducts();
    })
    .catch(function (err) {
      console.log(err);
    });
};

//fuction 2 : hien thi san pham ra man hinh
const renderProducts = (list = undefined) => {
  let htmlContent = "";
  if (list === undefined) {
    for (let i in productList) {
      htmlContent += productList[i].render();
    }
  } else {
    for (let i in list) {
      htmlContent += list[i].render();
    }
  }

  document.getElementById("UI_sanPham").innerHTML = htmlContent;
};

//function 3: map tu ds san pham cua backend ra thanh doi tuong san pham cua minh

const mapData = (data) => {
  for (let item of data) {
    let myProductObject;
    myProductObject = new DienThoai(
      item.id,
      item.name,
      item.image,
      item.description,
      item.price,
      item.inventory,
      item.rating,
      item.type
    );
    productList.push(myProductObject);
  }
  console.log(productList);
};

//function 1: theem sarn pham
const addProduct = () => {
  const typeProduct = document.getElementById("type").value;
  const idProduct = document.getElementById("id").value;
  const nameProduct = document.getElementById("ten").value;
  const imgProduct = document.getElementById("hinhAnh").value;
  const desProduct = document.getElementById("mota").value;
  const priceProduct = document.getElementById("giatien").value;
  const quantityProduct = document.getElementById("soluong").value;
  const ratingProduct = document.getElementById("rating").value;

  const dienThoai = new DienThoai(
    idProduct,
    nameProduct,
    imgProduct,
    desProduct,
    priceProduct,
    quantityProduct,
    ratingProduct,
    typeProduct
  );

  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
    method: "POST",
    data: dienThoai,
  })
    .then(function (res) {
      console.log(res);
      fetchProducts();
      alert("Thêm sản phẩm thành công!!!");
    })
    .catch(function (err) {
      console.log(err);
    });
};

//function 2: xóa sản phẩm khỏi database

const deleteProduct = (idProduct) => {
  axios({
    url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${idProduct}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log(res);
      fetchProducts();
      alert("Đã xóa thành công!!");
    })
    .catch(function (err) {
      console.log(err);
    });
};

//sửa sản phẩm
let idUpdate;
const getUpdateProduct = (idProduct) => {
  axios({
    url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${idProduct}`,
    method: "GET",
  }).then(function (res) {
    console.log(res);
    const dienThoai = res.data;
    console.log(dienThoai);

    document.getElementById("type1").value = dienThoai.type;
    document.getElementById("id1").value = dienThoai.id;
    document.getElementById("ten1").value = dienThoai.name;
    document.getElementById("hinhAnh1").value = dienThoai.image;
    document.getElementById("mota1").value = dienThoai.description;
    document.getElementById("giatien1").value = dienThoai.price;
    document.getElementById("soluong1").value = dienThoai.inventory;
    document.getElementById("rating1").value = dienThoai.rating;
    // chan o id ko cho ng dung chinh sua id
    document.getElementById("id1").setAttribute("disabled", true);

    idUpdate = dienThoai.id;
    console.log(idUpdate);
  });
  let htmlContent = "";
  htmlContent += `
  <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">đóng</button>
                <button type="button" class="btn btn-primary btn-success" data-dismiss="modal"
                onclick="updateInfo(idUpdate)">Update sản phẩm</button>
            </div>
  `;
  document.getElementById("btnInner").innerHTML = htmlContent;
};
const updateInfo = (idUpdate) => {
  console.log(idUpdate);
  const type = document.getElementById("type1").value;
  const id = document.getElementById("id1").value;
  const name = document.getElementById("ten1").value;
  const image = document.getElementById("hinhAnh1").value;
  const description = document.getElementById("mota1").value;
  const price = document.getElementById("giatien1").value;
  const inventory = document.getElementById("soluong1").value;
  const rating = document.getElementById("rating1").value;
  const dienThoaiDaUpdate = new DienThoai(
    id,
    name,
    image,
    description,
    price,
    inventory,
    rating,
    type
  );

  axios({
    url: `https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products/${idUpdate}`,
    method: "PUT",
    data: dienThoaiDaUpdate,
  }).then(function (res) {
    console.log(res);
    fetchProducts();
  });
};
// document.getElementById("updateBtn").addEventListener("click",updateInfo())
fetchProducts();
